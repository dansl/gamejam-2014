﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(MoveObject))]
public class MoveObjectEditor : Editor {


    public override void OnInspectorGUI()
    {
        MoveObject myTarget = target as MoveObject;

        if (GUILayout.Button("Add Transform"))
        {
            myTarget.transforms.Add(new Vector3(myTarget.transform.position.x, myTarget.transform.position.y, myTarget.transform.position.z));
        }

        if (GUILayout.Button("Advance State"))
        {
            myTarget.AdvanceState();
        }

        if (GUILayout.Button("Delete Current State"))
        {
            myTarget.DeleteCurrentState();
        }

        DrawDefaultInspector();

    }
}
