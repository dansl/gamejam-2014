﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(MoveScale))]

public class MoveScaleEditor : Editor 
{
	public override void OnInspectorGUI()	
	{
		MoveScale myTarget = target as MoveScale;
		
		DrawDefaultInspector();
		
		if (GUILayout.Button("Add Transform"))	
		{
			myTarget.scales.Add(new Vector3(myTarget.transform.localScale.x, myTarget.transform.localScale.y, myTarget.transform.localScale.z));
			myTarget.transforms.Add(new Vector3(myTarget.transform.position.x, myTarget.transform.position.y, myTarget.transform.position.z));
		}
		if (GUILayout.Button("Advance State"))	
		{
			myTarget.AdvanceState();
		}
		if (GUILayout.Button ("Remove State")) 
		{
			myTarget.RemoveState();
		}
	}	
}
