﻿using UnityEngine;
using System.Collections;

public class ToggleMusic : MonoBehaviour {
	
	public KeyCode toggleMusic;
	public AudioClip clip1;

	// Use this for initialization
	void Start () 
	{
		audio.clip = clip1;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (audio.isPlaying && Input.GetKeyDown(toggleMusic)) 
		{
			audio.Stop ();
		} 
		else if(!audio.isPlaying && Input.GetKeyDown(toggleMusic))
		{
			audio.Play ();
		}
	}
}
