﻿using UnityEngine;
using System.Collections;

public class PlayerSoundFX : MonoBehaviour {

	bool isMoving = false;
	bool canPlay = true;

	float soundTimer = 0.0f;

	public AudioClip walkSound;

	// Use this for initialization
	void Start () {
		audio.clip = walkSound;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.S)) 
		{
			isMoving = true;
			soundTimer += Time.deltaTime;
		} 
		else 
		{
			isMoving = false;
		}
		if (soundTimer >= 0.5) 
		{
			soundTimer = 0.0f;
			canPlay = true;
		}
		else if (soundTimer <= 0.4) 
		{
			canPlay = false;
		}

		if (isMoving && canPlay) 
		{
			audio.PlayOneShot (walkSound);
		} 
	}
}
