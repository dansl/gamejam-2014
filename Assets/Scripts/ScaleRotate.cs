﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScaleRotate : BaseObject {
	
	public List<Vector3> scales = new List<Vector3>();
	
	// Use this for initialization
	
	public void RemoveState()
	{
        transforms.Remove(transforms[currentState]);
		scales.Remove (scales [currentState]);
	}

    protected override void RefreshState()
    {
        transform.rotation = new Quaternion(transforms[currentState].x, transforms[currentState].y, transforms[currentState].z, 1);
        transform.localScale = scales[currentState];

		
    }

    protected override void UpdateTransition()
    {
		Quaternion oldRotation = new Quaternion(transforms[currentState].x, transforms[currentState].y, transforms[currentState].z, 1);
		Quaternion newRotation = new Quaternion(transforms[NextState()].x, transforms[NextState()].y, transforms[NextState()].z, 1);
		Vector3 oldScale = scales[currentState];
		Vector3 newScale = scales [NextState ()];


		this.transform.rotation = Quaternion.Slerp(oldRotation, newRotation, transitionValue);
		this.transform.localScale = Vector3.Lerp (oldScale, newScale, transitionValue);
    }
}