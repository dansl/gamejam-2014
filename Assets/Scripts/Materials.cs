﻿using UnityEngine;
using System.Collections;

public class Materials {

    public Material RedMaterial;
    public Material GreenMaterial;
    public Material BlueMaterial;
    public Material MagentaMaterial;
    public Material CyanMaterial;
    public Material YellowMaterial;
    public Material GrayMaterial;
    public Material PartialRedMaterial;
    public Material PartialBlueMaterial;
    public Material PartialGreenMaterial;
    
    private Materials()
    {
    }

    public void Initialize(MaterialsDefinitions source)
    {
        this.RedMaterial = source.RedMaterial;
        this.GreenMaterial = source.GreenMaterial;
        this.BlueMaterial = source.BlueMaterial;
        this.MagentaMaterial = source.MagentaMaterial;
        this.CyanMaterial = source.CyanMaterial;
        this.YellowMaterial = source.YellowMaterial;
        this.GrayMaterial = source.GrayMaterial;
        this.PartialRedMaterial = source.PartialRedMaterial;
        this.PartialBlueMaterial = source.PartialBlueMaterial;
        this.PartialGreenMaterial = source.PartialGreenMaterial;
    }

    private static Materials instance = null;

    public static Materials Instance {
        get
        {
            if (instance == null) instance = new Materials();
            return instance;
        }
    }

	
}
