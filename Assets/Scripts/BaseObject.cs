﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BaseObject : MonoBehaviour {

    public int currentState = 0;
    protected bool forwardCycle = true;
    public List<Vector3> transforms = new List<Vector3>();

    protected bool inTransition = false;
    public float transitionTime = 1.0f;
    public float transitionValue = 1.0f;


    // Use this for initialization
	void Start () {
        RefreshState();
        OnStateChanged();
	}
	
	// Update is called once per frame
	void Update () {

        if (inTransition)
        {
            transitionValue += Time.deltaTime;

            // Abstract, overridden in base classes
            UpdateTransition();

            if (transitionValue >= 1.0f)
            {
                inTransition = false;
                currentState = NextState();
                OnStateChanged();
                transitionValue = 0.0f;
            }
        }
    
    }

    protected abstract void UpdateTransition();

    public void DoInteraction()
    {
        if (inTransition) return;
        transitionValue = 0.0f;
        inTransition = true;
    }

    protected int NextState()
    {
        //print(string.Format("Forward is {0}, current state is {1}, state count is {2}", forwardCycle, currentState, transforms.Count));
        if (transforms.Count == 0) return 0;
        if (forwardCycle && currentState < transforms.Count - 1)
        {
            return currentState + 1;
        }
        else if (!forwardCycle && currentState > 0)
        {
            return currentState - 1;
        }
        return currentState;
    }

    public void OnStateChanged()
    {
        if (forwardCycle && currentState == transforms.Count - 1)
        {
            forwardCycle = false;
        }
        else if (!forwardCycle && currentState == 0)
        {
            forwardCycle = true;
        }
    }

    public void AdvanceState()
    {
        print(string.Format("AdvanceState: Forward is {0}, current state is {1}, state count is {2}", forwardCycle, currentState, transforms.Count));
        if (transforms.Count == 0) return;

        if (currentState == 0)
        {
            forwardCycle = true;
        }
        else if (currentState == transforms.Count - 1)
        {
            print("At end of list, reversing direction");
            forwardCycle = false;
        }

        if (currentState >= 0 && currentState <= transforms.Count - 1)
        {
            if (forwardCycle == true)
            {
                currentState++;
                OnStateChanged();
            }
            else if (forwardCycle == false)
            {
                currentState--;
                OnStateChanged();
            }
        }

        RefreshState();
    }

    protected abstract void RefreshState();

}
