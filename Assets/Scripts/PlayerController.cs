﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public GameObject MainCamera;

	public float MovementSpeed = 15.0f;
	public float Gravity = 50.0f;

	public float MouseSensitivityX = 5.0f;
	public float MouseSensitivityY = 2.0f;

	public GameObject RedOrbInHand;
	public GameObject BlueOrbInHand;
	public GameObject GreenOrbInHand;

	public GameObject RedDoor;
	public GameObject BlueDoor;
	public GameObject GreenDoor;

	public AudioClip blockHit;

	public GameObject StartPosition;

    public VisibleColor.Colors PlayerVisibleColor = VisibleColor.Colors.All;

	private float RotateY = 0.0f;

    public bool RedRoomComplete = false;
    public bool GreenRoomComplete = false;
    public bool BlueRoomComplete = false;

	// Use this for initialization
	void Start () {
		RedOrbInHand.SetActive(false);
		BlueOrbInHand.SetActive(false);
		GreenOrbInHand.SetActive(false);

		audio.clip = blockHit;
	}
	
	// Update is called once per frame
	void Update () {

		RaycastHit hit;
		if (!Physics.Raycast(transform.position, -transform.up, out hit, 2.0F))
		{
			Debug.Log("NOT GROUNDED!");
			rigidbody.AddForce(-transform.up * Gravity);
		}

		//Player movement with arrow keys
		if(Input.GetAxis("Vertical") > 0){
			rigidbody.AddForce(transform.forward * MovementSpeed);
		}else if(Input.GetAxis("Vertical") < 0){
			rigidbody.AddForce(transform.forward * -MovementSpeed);
		}
		if(Input.GetAxis("Horizontal") > 0){
			rigidbody.AddForce(transform.right * MovementSpeed);
		}else if(Input.GetAxis("Horizontal") < 0){
			rigidbody.AddForce(transform.right * -MovementSpeed);
		}

		//Rotate Player
		transform.Rotate(0, Input.GetAxis("Mouse X") * MouseSensitivityX, 0);

		//Camera movement
		RotateY += -Input.GetAxis("Mouse Y")*MouseSensitivityY;
		RotateY = Mathf.Clamp(RotateY, -60, 60);
		//Debug.Log ("RotateY "+RotateY);
		MainCamera.transform.localEulerAngles = new Vector3(RotateY, 0, 0);

		//Click remove cursor
		if(Input.GetMouseButtonDown(0)){
			Screen.lockCursor = true;
			Screen.showCursor = false;
			Punch ();
		}

		//Esc show cursor
		if(Input.GetKeyDown("escape")){
			Screen.lockCursor = false;
			Screen.showCursor = true;
			Application.LoadLevel("Main Menu");
		}
	}

	public void SetVisibleLight(VisibleColor.Colors color){
		PlayerVisibleColor = color;
		Debug.Log ("Player Visible Light Set To: "+color);
		if(color == VisibleColor.Colors.Red){
			RedOrbInHand.SetActive(true);
			BlueOrbInHand.SetActive(false);
			GreenOrbInHand.SetActive(false);
			if(RedDoor != null){
				RedDoor.SetActive(false);
				BlueDoor.SetActive(true);
				GreenDoor.SetActive(true);
			}
		}else if(color == VisibleColor.Colors.Blue){
			RedOrbInHand.SetActive(false);
			BlueOrbInHand.SetActive(true);
			GreenOrbInHand.SetActive(false);
			if(RedDoor != null){
				RedDoor.SetActive(true);
				BlueDoor.SetActive(false);
				GreenDoor.SetActive(true);
			}
		}else if(color == VisibleColor.Colors.Green){
			RedOrbInHand.SetActive(false);
			BlueOrbInHand.SetActive(false);
			GreenOrbInHand.SetActive(true);
			if(RedDoor != null){
				RedDoor.SetActive(true);
				BlueDoor.SetActive(true);
				GreenDoor.SetActive(false);
			}
		}else if(color == VisibleColor.Colors.None){
			RedOrbInHand.SetActive(false);
			BlueOrbInHand.SetActive(false);
			GreenOrbInHand.SetActive(false);
			if(RedDoor != null){
				RedDoor.SetActive(true);
				BlueDoor.SetActive(true);
				GreenDoor.SetActive(true);
			}
		}
	}

	public void ResetToStart(){
		transform.localPosition = StartPosition.transform.localPosition;
		transform.localRotation = StartPosition.transform.localRotation;
	}

	public void SetRoomComplete(VisibleColor.Colors color){
		if(color == VisibleColor.Colors.Red){
			RedRoomComplete = true;
		}else if(color == VisibleColor.Colors.Blue){
			BlueRoomComplete = true;
		}else if(color == VisibleColor.Colors.Green){
			GreenRoomComplete = true;
		} 
	}

	void Punch(){
		//Animate Punch and actions.
		animation.Play();

		RaycastHit hit;
        if (Physics.Raycast(MainCamera.transform.position, MainCamera.transform.forward, out hit, 6.0F))
        {
			audio.PlayOneShot (blockHit);

            Debug.Log("HIT OBJ " + hit.transform.name);
            if (hit.transform.tag == "InteractiveObject")
            {
                Debug.Log("HIT CUBE!");
                hit.transform.gameObject.GetComponent<BaseObject>().DoInteraction();
            }
        }
        
        else
        {
            print("no hit");
        }
	}
}
