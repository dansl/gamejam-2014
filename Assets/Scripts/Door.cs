﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public int curPosition = 0;

	public Vector3 Location1POS;
	public Vector3 Location1ROT;

	public Vector3 Location2POS;
	public Vector3 Location2ROT;

	private Vector3 curPOS;
	private Vector3 curROT;


	// Use this for initialization
	void Start () {
		curPOS = Location1POS;
		curROT = Location1ROT;
	}
	
	// Update is called once per frame
	void Update () {
		if(curPosition == 0){
			curPOS = Location1POS;
			curROT = Location1ROT;
		}else if(curPosition == 1){
			curPOS = Location2POS;
			curROT = Location2ROT;
		}

		transform.localPosition = Vector3.Lerp(transform.localPosition, curPOS, Time.deltaTime*2);
		transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, curROT, Time.deltaTime*2);
	}

	public void SwapPOS(int num){
		curPosition = num;
	}
}
