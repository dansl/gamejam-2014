﻿using UnityEngine;
using System.Collections;

public class MaterialManager : MonoBehaviour {

    private GameObject player = null;

    private Material redMaterial;
    private Material greenMaterial;
    private Material blueMaterial;
    private Material magentaMaterial;
    private Material cyanMaterial;
    private Material yellowMaterial;
    private Material grayMaterial;

    private VisibleColor.Colors lastPlayerColor = VisibleColor.Colors.None;

    public enum ObjectTrueColor
    {
        Red,
        Green,
        Blue,
        Yellow,
        Magenta,
        Cyan
    }

    public ObjectTrueColor trueColor;

	// Use this for initialization
	void Start () {

        player = GameObject.FindGameObjectWithTag("Player");

	}
	
	// Update is called once per frame
	void Update () {

        if (player.GetComponent<PlayerController>().PlayerVisibleColor != lastPlayerColor)
        {
			lastPlayerColor = player.GetComponent<PlayerController>().PlayerVisibleColor;

			switch (player.GetComponent<PlayerController>().PlayerVisibleColor)
            {
                case VisibleColor.Colors.All:
                    AssignTrueMaterial();
                    break;
				case VisibleColor.Colors.Blue:
                    AssignBlueMaterial();
                    break;
				case VisibleColor.Colors.Green:
                    AssignGreenMaterial();
                    break;
				case VisibleColor.Colors.Red:
                    AssignRedMaterial();
                    break;
				case VisibleColor.Colors.None:
                    AssignGrayMaterial();
                    break;
            }
        }



	}

    private void AssignMaterial(Material simulatedMaterial)
    {
        this.GetComponent<MeshRenderer>().material = simulatedMaterial;
    }

    private void AssignGrayMaterial()
    {
        AssignMaterial(Materials.Instance.GrayMaterial);
    }

    private void AssignRedMaterial()
    {
        switch (this.trueColor)
        {
            case ObjectTrueColor.Red:
                AssignMaterial(Materials.Instance.RedMaterial);
                break;
            case ObjectTrueColor.Magenta:
                AssignMaterial(Materials.Instance.PartialRedMaterial);
                break;
            case ObjectTrueColor.Yellow:
                AssignMaterial(Materials.Instance.PartialRedMaterial);
                break;
            default:
                AssignMaterial(Materials.Instance.GrayMaterial);
                break;
        }
    }

    private void AssignGreenMaterial()
    {
        switch (this.trueColor)
        {
            case ObjectTrueColor.Green:
                AssignMaterial(Materials.Instance.GreenMaterial);
                break;
            case ObjectTrueColor.Cyan:
                AssignMaterial(Materials.Instance.PartialGreenMaterial);
                break;
            case ObjectTrueColor.Yellow:
                AssignMaterial(Materials.Instance.PartialGreenMaterial);
                break;
            default:
                AssignMaterial(Materials.Instance.GrayMaterial);
                break;
        }
    }

    private void AssignBlueMaterial()
    {
        switch (this.trueColor)
        {
            case ObjectTrueColor.Blue:
                AssignMaterial(Materials.Instance.GreenMaterial);
                break;
            case ObjectTrueColor.Cyan:
                AssignMaterial(Materials.Instance.PartialBlueMaterial);
                break;
            case ObjectTrueColor.Magenta:
                AssignMaterial(Materials.Instance.PartialBlueMaterial);
                break;
            default:
                AssignMaterial(Materials.Instance.GrayMaterial);
                break;
        }
    }

    private void AssignTrueMaterial()
    {
        switch (this.trueColor)
        {
            case ObjectTrueColor.Blue:
                AssignMaterial(Materials.Instance.BlueMaterial);
                break;
            case ObjectTrueColor.Cyan:
                AssignMaterial(Materials.Instance.CyanMaterial);
                break;
            case ObjectTrueColor.Green:
                AssignMaterial(Materials.Instance.GreenMaterial);
                break;
            case ObjectTrueColor.Magenta:
                AssignMaterial(Materials.Instance.MagentaMaterial);
                break;
            case ObjectTrueColor.Red:
                AssignMaterial(Materials.Instance.RedMaterial);
                break;
            case ObjectTrueColor.Yellow:
                AssignMaterial(Materials.Instance.YellowMaterial);
                break;
        }
    }
}
