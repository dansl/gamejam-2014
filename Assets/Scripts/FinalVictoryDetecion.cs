﻿using UnityEngine;
using System.Collections;

public class FinalVictoryDetecion : MonoBehaviour {

    private bool hasFinished = false;
	private PlayerController playerController;

	// Use this for initialization
	void Start () {
		playerController = this.gameObject.GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {

        

        if (!hasFinished && playerController.BlueRoomComplete && playerController.GreenRoomComplete && playerController.RedRoomComplete)
        {
            RaycastHit hit;
            if (Physics.Raycast(new Ray(Camera.main.transform.position, Camera.main.transform.forward), out hit))
            {
                if (hit.collider.tag == "Finish")
                {
                    hasFinished = true;

                    GameObject rootObject = GameObject.Find("aurora_take4");

                    foreach (Transform puzzlePiece in rootObject.transform)
                    {
                        print("Altering " + puzzlePiece.name);
                        switch (puzzlePiece.tag)
                        {
                            case "Move":
                                puzzlePiece.GetComponent<MoveObject>().DoInteraction();
                                break;
                            case "Rotate":
                                puzzlePiece.GetComponent<RotateObject>().DoInteraction();
                                break;
                            case "Scale":
                                puzzlePiece.GetComponent<ScaleObject>().DoInteraction();
                                break;
                        }

                    }


                }

            }


        }

	}
}
