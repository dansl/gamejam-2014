﻿using UnityEngine;
using System.Collections;

public class LookUpBox : MonoBehaviour {

	private PlayerController playerController;
	
	// Use this for initialization
	void Start () {
		playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		if(playerController.BlueRoomComplete && playerController.GreenRoomComplete && playerController.RedRoomComplete){
			renderer.enabled = true;
		}else{
			renderer.enabled = false;
		}
	}
}
