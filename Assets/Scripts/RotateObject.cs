﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RotateObject : BaseObject {

	// Use this for initialization
    public List<Quaternion> rotations = new List<Quaternion>();

    public int correctState = 0;

    public bool IsInCorrectState
    {
        get
        {
            return currentState == correctState;
        }
    }

	// Update is called once per frame
	
	public void RemoveState()
	{
		transforms.Remove (transforms [currentState]);
        rotations.Remove(rotations[currentState]);
	}

    protected override void RefreshState()
    {
		if(rotations.Count > 0)
       	 transform.rotation = rotations[currentState];
    }

    protected override void UpdateTransition()
    {
        //print(string.Format("Rotate object clicked: Forward is {0}, current state is {1}, number of states is {2}", this.forwardCycle, this.currentState, this.transforms.Count));

        // this.transform.position = Vector3.Lerp(transforms[currentState], transforms[NextState()], transitionValue);
		if(rotations.Count > 0){
        	Quaternion oldRotation = rotations[currentState];
        	Quaternion newRotation = rotations[NextState()];

        	this.transform.rotation = Quaternion.Slerp(oldRotation, newRotation, transitionValue);
		}

    }
}