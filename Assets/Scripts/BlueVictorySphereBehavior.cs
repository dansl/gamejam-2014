﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlueVictorySphereBehavior : ColorPowerUp {

    private List<GameObject> eyeObjects = new List<GameObject>();

    private Vector3 finalPosition;

	// Use this for initialization
	void Start () {
		base.Start();

        GameObject[] allInteractiveObjects = GameObject.FindGameObjectsWithTag("InteractiveObject");
        foreach (GameObject interactiveObject in allInteractiveObjects)
        {
            if (interactiveObject.name == "Cube-BLUE")
            {
                eyeObjects.Add(interactiveObject);
            }
        }

        print(eyeObjects.Count.ToString() + " eye objects found");

        finalPosition = this.transform.position;

	}
	
	// Update is called once per frame
	void Update () 
    {
        bool correct = true;
        foreach (GameObject eyeObject in eyeObjects)
        {
            if (!eyeObject.GetComponent<RotateObject>().IsInCorrectState)
            {
                correct = false;
                break;
            }
        }

        if (correct)
        {
            this.transform.position = finalPosition;
        }
        else
        {
            this.transform.position = finalPosition * 1000;
        }

	}

}
