﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(ScaleObject))]

public class ScaleObjectEditor : Editor 
{
	public override void OnInspectorGUI()	
	{
		ScaleObject myTarget = target as ScaleObject;
		
		DrawDefaultInspector();
		
		if (GUILayout.Button("Add Transform"))	
		{
			myTarget.transforms.Add(new Vector3(myTarget.transform.localScale.x, myTarget.transform.localScale.y, myTarget.transform.localScale.z));	
		}
		if (GUILayout.Button("Advance State"))	
		{
			myTarget.AdvanceState();
		}
		if (GUILayout.Button ("Remove State")) 
		{
			myTarget.RemoveState ();
		}
	}	
}