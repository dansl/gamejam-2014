﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(RotateObject))]

public class RotateObjectEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		RotateObject myTarget = target as RotateObject;
		
		DrawDefaultInspector();

		if (GUILayout.Button("Add Transform"))
		{
            myTarget.rotations.Add(myTarget.transform.rotation);
            myTarget.transforms.Add(Vector3.zero);
		}
		if (GUILayout.Button("Advance State"))	
		{
			myTarget.AdvanceState();
		}
		if (GUILayout.Button ("Remove State")) 
		{
			myTarget.RemoveState();
		}
	}	
}