﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MoveRotateObject))]
public class MoveRotateObjectEditor : Editor {

    public override void OnInspectorGUI()
    {
        MoveRotateObject myTarget = target as MoveRotateObject;

        if (GUILayout.Button("Add Transform"))
        {
            myTarget.transforms.Add(new Vector3(myTarget.transform.position.x, myTarget.transform.position.y, myTarget.transform.position.z));
            myTarget.rotateTransforms.Add(new Vector3(myTarget.transform.rotation.x, myTarget.transform.rotation.y, myTarget.transform.rotation.z));
        }

        if (GUILayout.Button("Advance State"))
        {
            myTarget.AdvanceState();
        }

        if (GUILayout.Button("Delete Current State"))
        {
            myTarget.DeleteCurrentState();
        }

        DrawDefaultInspector();

    }

}
