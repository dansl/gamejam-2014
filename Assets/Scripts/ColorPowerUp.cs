﻿using UnityEngine;
using System.Collections;

public class ColorPowerUp : MonoBehaviour {

	public VisibleColor.Colors LightColor;

	public bool IsFinal = false;

	private float bobbingSpeed = 5f;
	private float bobbingAmount = 0.03f;

	private GameObject[] AllColorOrbs;

	public bool OrbEnabled = true;
	public bool OrbTerminated = false; //Use for level completeion

	private PlayerController Player;

	// Use this for initialization
	protected void Start () {
		AllColorOrbs = GameObject.FindGameObjectsWithTag("ColorOrb");
		Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		//Bobbing Script

		transform.localPosition = new Vector3(transform.localPosition.x, (Mathf.Sin(Time.time*bobbingSpeed)*bobbingAmount) + transform.localPosition.y, transform.localPosition.z);
	
	}

	void OnTriggerEnter(Collider obj) {
		Debug.Log ("Collide "+obj.transform.tag);
		if(OrbEnabled){
			if(obj.transform.tag == "Player"){

				audio.Play();

				if(!IsFinal){
					DisableOrb(false); //Disable this orb
					obj.transform.GetComponent<PlayerController>().SetVisibleLight(LightColor);
					foreach (GameObject orb in AllColorOrbs){
						ColorPowerUp temp = orb.GetComponent<ColorPowerUp>();
						if(orb != gameObject){
							temp.EnableOrb();//Enable other Orbs
						}
					}
				}else if(IsFinal){
					DisableOrb(true); //Disable this orb
					foreach (GameObject orb in AllColorOrbs){
						ColorPowerUp temp = orb.GetComponent<ColorPowerUp>();
						if(temp.LightColor == LightColor){
							temp.DisableOrb(true); //Terminate the other orb from the beginning so it can never return
							Player.SetVisibleLight(VisibleColor.Colors.None);
							Player.SetRoomComplete(LightColor);
							Player.ResetToStart();
						}
					}
				}

			}
		}
	}

	void DisableOrb(bool terminate){
		OrbTerminated = terminate;

		gameObject.collider.enabled = false;
		gameObject.renderer.enabled = false;
		gameObject.light.enabled = false;
		(gameObject.GetComponent("Halo") as Behaviour).enabled = false;
		OrbEnabled = false;
	}

	void EnableOrb(){
		if(!OrbTerminated){
			gameObject.collider.enabled = true;
			gameObject.renderer.enabled = true;
			gameObject.light.enabled = true;
			(gameObject.GetComponent("Halo") as Behaviour).enabled = true;
			OrbEnabled = true;
		}
	}
}
