using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveScale : BaseObject {
	
	public List<Vector3> scales = new List<Vector3>();
	
	// Use this for initialization
	
	public void RemoveState()
	{
        transforms.Remove(transforms[currentState]);
		scales.Remove (scales [currentState]);
	}

    protected override void RefreshState()
    {
        transform.position = transforms[currentState];
        transform.localScale = scales[currentState];
    }

    protected override void UpdateTransition()
    {
		Vector3 oldScale = new Vector3(scales[currentState].x, scales[currentState].y, scales[currentState].z);
		Vector3 newScale = new Vector3(scales[NextState()].x, scales[NextState()].y, scales[NextState()].z);

		this.transform.localScale = Vector3.Slerp(oldScale, newScale, transitionValue);
		this.transform.position = Vector3.Lerp(transforms[currentState], transforms[NextState()], transitionValue);
    }
}