﻿using UnityEngine;
using System.Collections;

public class Elevator : MonoBehaviour {

	public Vector3 POS1;
	public Vector3 POS2;

	public int CurPosition = 0;
	public float speed = 0.05F;

	private float journeyLength;
	private Vector3 curPOS;

	private float startTime;
	// Use this for initialization
	void Start () {
		journeyLength = Vector3.Distance(POS1, POS2);
	}
	
	// Update is called once per frame
	void Update () {
		if(CurPosition == 0){
			curPOS = POS1;

		}else if(CurPosition == 1){
			curPOS = POS2;

		}

		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;
		transform.parent.localPosition = Vector3.Lerp(transform.parent.localPosition, curPOS, fracJourney);
	}

	void SwapPOS(int num){
		Debug.Log ("SWAP POS");
		startTime = Time.time;
		CurPosition = num;
	}

	void OnTriggerEnter(Collider obj){
		Debug.Log ("ENTER "+obj.transform.tag);
		if(obj.transform.tag == "Player"){
			obj.transform.parent = transform.parent;

			StartCoroutine(RaiseElevator());
		}
	}

	void OnTriggerExit(Collider obj){
		Debug.Log ("ENTER "+obj.transform.tag);
		if(obj.transform.tag == "Player"){
			obj.transform.parent = null;
		}
	}

	IEnumerator RaiseElevator(){
		yield return new WaitForSeconds(0.5f);
		SwapPOS(1);
		StartCoroutine(DropElevator());
	}

	IEnumerator DropElevator(){
		yield return new WaitForSeconds(6);
		SwapPOS(0);
	}

	/*void OnCollisionExit(Collision obj){
		Debug.Log ("EXIT "+obj.transform.tag);
		if(obj.transform.tag == "Player"){
			//obj.transform.parent = null;
			//SwapPOS(0);
		}
	}*/


}
