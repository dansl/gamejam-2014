﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveObject : BaseObject
{
    
    public void DeleteCurrentState()
    {
        transforms.RemoveAt(currentState);
        if (transforms.Count == 0)
            currentState = 0;
        else
            if (currentState >= transforms.Count) currentState = transforms.Count - 1;
        RefreshState();
    }

    protected override void RefreshState()
    {
		if(transforms.Count > 0)
        	transform.position = transforms[currentState];
    }

    protected override void UpdateTransition()
    {
        this.transform.position = Vector3.Lerp(transforms[currentState], transforms[NextState()], transitionValue);
    }
}
