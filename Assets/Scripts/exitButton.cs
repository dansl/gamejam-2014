﻿using UnityEngine;
using System.Collections;

public class exitButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetMouseButtonDown(0)) 
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                if (hit.transform.gameObject == this.gameObject)
                {
                    Application.Quit(); // Exits the game.
                }
            }
        }

	}
}
