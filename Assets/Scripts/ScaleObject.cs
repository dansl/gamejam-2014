﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScaleObject : BaseObject {
	
	// Use this for initialization
	
	// Update is called once per frame
	
	public void RemoveState()
	{
		transforms.Remove (transforms [currentState]);
	}

    protected override void RefreshState()
    {
        transform.localScale = transforms[currentState];
    }

    protected override void UpdateTransition()
    {
        Vector3 oldScale = new Vector3(transforms[currentState].x, transforms[currentState].y, transforms[currentState].z);
        Vector3 newScale = new Vector3(transforms[NextState()].x, transforms[NextState()].y, transforms[NextState()].z);

        this.transform.localScale = Vector3.Slerp(oldScale, newScale, transitionValue);
    }
}