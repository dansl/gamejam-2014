﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(ScaleRotate))]

public class ScaleRotateEditor : Editor 
{
	public override void OnInspectorGUI()	
	{
		ScaleRotate myTarget = target as ScaleRotate;
		
		DrawDefaultInspector();
		
		if (GUILayout.Button("Add Transform"))	
		{
			myTarget.scales.Add(new Vector3(myTarget.transform.localScale.x, myTarget.transform.localScale.y, myTarget.transform.localScale.z));
            myTarget.transforms.Add(new Vector3(myTarget.transform.rotation.x, myTarget.transform.rotation.y, myTarget.transform.rotation.z));

		}
		if (GUILayout.Button("Advance State"))	
		{
			myTarget.AdvanceState();
		}
		if (GUILayout.Button ("Remove State")) 
		{
			myTarget.RemoveState();
		}
	}	
}