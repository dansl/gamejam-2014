﻿using UnityEngine;
using System.Collections;

public class MaterialsDefinitions : MonoBehaviour {

    public Material RedMaterial;
    public Material GreenMaterial;
    public Material BlueMaterial;
    public Material MagentaMaterial;
    public Material CyanMaterial;
    public Material YellowMaterial;
    public Material GrayMaterial;
    public Material PartialRedMaterial;
    public Material PartialBlueMaterial;
    public Material PartialGreenMaterial;
    
    // Use this for initialization
	void Start () {

        Materials.Instance.Initialize(this);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
