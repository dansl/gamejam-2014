﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour {

	public GameObject Door;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider obj){
		//Debug.Log ("COLLIDER ENTERED");
		if(obj.transform.tag == "InteractiveObject")
			Door.GetComponent<Door>().SwapPOS(1);
	}

	void OnTriggerExit(Collider obj){
		if(obj.transform.tag == "InteractiveObject")
			Door.GetComponent<Door>().SwapPOS(0);
	}
}
