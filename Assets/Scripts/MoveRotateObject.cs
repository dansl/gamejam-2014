﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveRotateObject : BaseObject
{
    public List<Vector3> rotateTransforms = new List<Vector3>();

    public void DeleteCurrentState()
    {
        transforms.RemoveAt(currentState);
        rotateTransforms.RemoveAt(currentState);
        if (transforms.Count == 0)
            currentState = 0;
        else
            if (currentState >= transforms.Count) currentState = transforms.Count - 1;
        RefreshState();
    }

    protected override void RefreshState()
    {
        transform.position = transforms[currentState];
        transform.rotation = new Quaternion(rotateTransforms[currentState].x, rotateTransforms[currentState].y, rotateTransforms[currentState].z, 1);
    }


    protected override void UpdateTransition()
    {
		Quaternion oldRotation = new Quaternion(transforms[currentState].x, transforms[currentState].y, transforms[currentState].z, 1);
		Quaternion newRotation = new Quaternion(transforms[NextState()].x, transforms[NextState()].y, transforms[NextState()].z, 1);
		
		this.transform.rotation = Quaternion.Slerp(oldRotation, newRotation, transitionValue);
		this.transform.position = Vector3.Lerp(transforms[currentState], transforms[NextState()], transitionValue);
    }
}
